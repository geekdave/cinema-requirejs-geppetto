/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var MovieModel = Backbone.Model.extend({
        defaults: {
        }
    });

    return MovieModel;
});
