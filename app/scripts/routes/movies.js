/*global define*/

define([
    'jquery',
    'backbone',
    'obscura',
    'collections/movies',
    'views/layout',
    'views/moviesList',
    'views/chose',
    'views/genres',
    'views/details',
    'views/sort',
    'mock'
], function ($, Backbone, Obscura, Movies, Layout, MoviesList, ChoseView, GenresView, DetailsView, SortView, Mock) {
    'use strict';

    Mock.start();

    var layout, 
        movies,
        proxy,
        sortView,
        genresView,
        deferred;

    if (!proxy) {
      movies = new Movies();
      proxy = new Obscura(movies);
      deferred = movies.fetch();
    }

    var MoviesRouter = Backbone.Router.extend({
        routes: {
          "movies/:id":  "selectMovie",
          "":            "main"
        },

        main: function() {
          console.log("** main **");
          layout = Layout.getInstance();
      
          var moviesList = new MoviesList({collection: proxy});
          var choseMovie = new ChoseView();
      
          deferred.done(function(results) {
            layout.setList(moviesList);
            layout.setDetails(choseMovie);
            console.log(proxy.toJSON());
            var superset = new Backbone.Collection(results);
            if (!sortView) {
              sortView = new SortView({el: $("#controls"), collection: proxy});
              genresView = new GenresView({el: $("#filter-controls"), collection: proxy, superset: superset});
            }
          });
        },
      
        selectMovie: function(id) {
          layout = Layout.getInstance();
      
          var moviesList = new MoviesList({collection: movies});
      
          deferred.done(function(results) {
            movies.selectByID(id);
            console.log(movies.get(id).toJSON());
            var details = new DetailsView({model: movies.get(id)});
            layout.setDetails(details);
          });
        }


    });

    return MoviesRouter;
});
