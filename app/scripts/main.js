/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        obscura: {
          deps: [ 'backbone' ],
          exports: 'Backbone.Obscura'
        },
        mockjax: {
           deps: ['jquery']
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        obscura: '../bower_components/backbone.obscura/backbone.obscura',
        mockjax: '../bower_components/jquery-mockjax/jquery.mockjax'
    }
});

require([
  'backbone',
  'routes/movies'
], function (Backbone, MoviesRouter) {

    new MoviesRouter();

    Backbone.history.start();
});
