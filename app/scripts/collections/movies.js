/*global define*/

define([
    'underscore',
    'backbone',
    'models/movie'
], function (_, Backbone, MoviesModel) {
    'use strict';

    var MoviesCollection = Backbone.Collection.extend({
      model: MoviesModel,

      url: '/api/movies',

      // Unselect all models
      resetSelected: function() {
        this.each(function(model) {
            model.set({ selected: false}, {silent: true});
        });
      },

      // Select a specific model from the collection
      selectByID: function(id) {
        this.resetSelected();
        var movie = this.get(id);
        movie.set({'selected': true}, {silent: true});
        this.trigger('reset');
        return movie.id;
      },

      sortByTitle: function() {
        return this.sortBy('title');
      },

      sortByRating: function() {
          var sorted = this.sortBy(function(m) {
            return (10 - m.get('rating'));
          });
          return sorted;
      },

      sortByShowtime: function() {
          return this.sortBy('showtime');
      },

      log: function() {
         _.each(this.toJSON(), function(movie) {
           var showtime = new Date(movie.showtime * 1000);
           console.log(movie.title + "   " + showtime.toLocaleString() + "(" + movie.showtime + ")");
         });
      }

    });

    return MoviesCollection;
});
