/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var SortView = Backbone.View.extend({

        events: {
          'click #by_title': 'sortByTitle',
          'click #by_rating': 'sortByRating',
          'click #by_showtime': 'sortByShowtime',
        },
    
        sortByTitle: function(ev) {
          this.collection.setSort("title", "asc");
        },
    
        sortByRating: function(ev) {
          this.collection.setSort("rating", "desc");
        },
    
        sortByShowtime: function(ev) {
          this.collection.setSort("showtime", "asc");
        }
    });

    return SortView;
});
