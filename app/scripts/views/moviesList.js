/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'views/movie',
], function ($, _, Backbone, JST, MovieView) {
    'use strict';


     var MoviesList = Backbone.View.extend({

           el: "#overview",

           render: function() {
             var moviesView = this.movies.map(function(movie) {
               return (new MovieView({model : movie})).render().el;
             });
             this.$el.html(moviesView);
             return this;
           },

           initialize: function() {
             _.bindAll(this, "render");
             this.movies = this.collection;
             this.listenTo(this.movies, "reset", this.render);
           }
     });

    return MoviesList;
});
