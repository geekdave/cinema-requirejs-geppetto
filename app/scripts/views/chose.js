/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var ChoseView = Backbone.View.extend({
      template: JST['app/scripts/templates/chose.ejs'],
      render: function() {
        var tmpl = this.template;
        this.$el.html(tmpl);
        return this;
      }
    });

    return ChoseView;
});
