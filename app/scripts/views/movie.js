/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var MovieView = Backbone.View.extend({
        template: JST['app/scripts/templates/movie.ejs'],
        tagName: 'article',
        className: 'movie',
    
        events: {
            'click': 'handleClick'
        },
    
        handleClick: function(ev) {
          console.log(this.model);
          console.log(this.model.collection);
          console.log('event on ' + this.model.id);
          //this.model.collection.selectByID(this.model.id);
          this.trigger('select:movie', this.model.id);
        },
    
        render: function() {
          var tmpl = this.template;
          this.$el.html(tmpl(this.model.toJSON()));
          this.$el.toggleClass('selected', this.model.get('selected'));
          return this;
        },
    
        initialize: function() {
        }
    });

    return MovieView;
});
