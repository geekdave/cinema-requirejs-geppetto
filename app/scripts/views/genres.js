/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var GenresView = Backbone.View.extend({
        template: JST['app/scripts/templates/genres.ejs']
    });

    return GenresView;
});
