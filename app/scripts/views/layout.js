/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var LayoutView = Backbone.View.extend({
        template: JST['app/scripts/templates/layout.ejs'],
        el: "#movies",
    
    
        render: function() {
          console.log("render layout");
          this.$el.html(this.template);
          return this;
        },
    
        setDetails: function(view) {
          console.log(view.render().el);
          this.$el.find("#details").html(view.render().el);
        },
    
        setList: function(view) {
          console.log(this.$el);
          console.log(view.render().el);
          //this.$el.append(view.render().el);
        },
    
        initialize: function() {
          _.bindAll(this, "render");
          this.render();
        }
      });


     LayoutView.getInstance = function() {

       var instance;

       if (!instance) {
         instance = new LayoutView();
       }
       return instance;

     }

    return LayoutView;
});
